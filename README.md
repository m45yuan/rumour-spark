# Rumour-Spark

**Real-time Rumour Analysis with PySpark**

We built a real-time rumour analysis pipeline 
for tweets in PySpark, which utilizes Spark SQL, Spark ML and 
Spark Structured Streaming. We use a publicly available dataset of 
tweets that have been annotated for subtasks related to rumour
analysis. This dataset is provided by the organizer of RumourEval 
2019 competition, which consists of two subtasks, namely stance 
detection and rumour veracity classification. We train a traditional 
machine learning model using manually extracted features for the 
two tasks separately and compare our results to existing deep 
learning approaches. We then use one of our models to analyze
tweets being posted in real time using the PySpark Structured 
Streaming API. While most of the current research being done in 
NLP is focused on deep learning (which primarily uses GPU 
clusters), the sheer volume of social media posts means there is still 
value in training traditional machine learning models that can be 
run on massively parallel CPU clusters.

**Introduction**:   
https://www.youtube.com/watch?v=Ypkch7dtvwI

**Group Member Names**:  
Amir Vakili Tahami  
Meng Yuan  
Zhao Li



